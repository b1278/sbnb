from django.contrib import admin
from host.models import Listing,City,State,Country,Area,ListingPhoto,Host

admin.site.register(Listing)
admin.site.register(City)
admin.site.register(State)
admin.site.register(Country)
admin.site.register(Area)
admin.site.register(ListingPhoto)
admin.site.register(Host)