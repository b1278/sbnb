from django.shortcuts import redirect


def user_check(request):
	user = request.user
	if hasattr(user, 'host'):
		return redirect('host_dashboard')

	if hasattr(user, 'guest'):
		return redirect('guest_listings_home')

