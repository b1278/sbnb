import logging
from urllib.parse import urlparse, urlunparse
from django.contrib.auth.decorators import login_required,user_passes_test
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, QueryDict,Http404
from django.core.urlresolvers import reverse_lazy,reverse
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)

from django.views.generic import (
    FormView,
    ListView,
    DetailView,
    UpdateView,
    View,
)
from django.conf import settings
from django.shortcuts import render


from host.models import (
    Host,
    HostBankDetail,
    Listing,
    Bed,
    ListingPhoto,
    ListingPrice,
    ListingBusinessHour
)
from host.forms import (
    HostCreationForm,
    HostLoginForm,
    ListingForm,
    BedForm,
    ListingPhotoForm,
    HostUpdateForm,
    HostBankForm,
    ListingBusinessHourForm,
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from guest.models  import Booking


user_login_required = user_passes_test(lambda user:user.host, login_url='/host/login/')

def active_host_required(view_func):
    decorated_view_func = login_required(user_login_required(view_func))
    return decorated_view_func


class SuccessURLAllowedMixin:
    success_url_allowed_hosts = set()
    
    def get_success_url_allowed_hosts(self):
        return {self.request.get_host(), *self.success_url_allowed_hosts}
    

    
class HostCreateView(SuccessURLAllowedMixin, FormView):
    """
    Creates The New Host.
    """
    form_class = HostCreationForm
    template_name = 'host/host_creation.html'
    
    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:            
            return HttpResponseRedirect(reverse('guest_search_listings_view'))
        return super().dispatch(request, *args, **kwargs)
    
    def get_success_url(self):
        return redirect('host_thank_you_view')
    
    def get_form_class(self):
        return self.form_class
    
    def form_valid(self, form):
        instance = form.save()
        return HttpResponseRedirect(reverse('host_thank_you_view'))
    
    def form_invalid(self, form):
        print(form)
        return self.render_to_response(self.get_context_data(form=form))
    
    
host_creation_view = HostCreateView.as_view()


def host_thank_you_view(request):
    return render(request, 'host/host_thank_you.html')


class HostLoginView(LoginView):    
    def form_valid(self, form):
        print(form.get_user())
        request_user = form.get_user()
        if hasattr(request_user, 'host'):
            try:
                host = Host.objects.get(user=request_user)
            except Host.DoesNotExist:
                pass
            if host.is_active:
                auth_login(self.request, form.get_user())        
                return HttpResponseRedirect(self.get_success_url())
            else:
                return HttpResponseRedirect(reverse('host_thank_you_view'))
        else:
            return redirect('guest_listings_home')
    
host_login_view = HostLoginView.as_view()


class HostLogoutView(LoginRequiredMixin,View):
    next_page = reverse_lazy('host_login_view')

    @method_decorator(never_cache)
    def dispatch(self,request, *args, **kwargs):
        auth_logout(request)
        return HttpResponseRedirect(reverse('host_login_view'))

host_logout_view = HostLogoutView.as_view()


class HostNewListingView(FormView):
    form_class = ListingForm
    template_name = 'host/host_new_listing.html'
    
    def form_valid(self, form):
        listing = form.save(commit=False)
        listing.host = self.request.user.host
        listing.save()
        listing_price = ListingPrice.objects.create(
            segmentation=self.request.POST['segmentation'],
            min_price=self.request.POST['min_price'],
            max_price=self.request.POST['max_price'],
            listing=listing
        )
        print(self.request.POST)
        #send_congrats_notification.delay()
        #send_admin_notification.delay()

        return redirect('host_listing_lists_view')
    
    def form_invalid(self, form):
        print("invalid")
        return self.render_to_response(self.get_context_data(form=form))
    
    def get_form_class(self):
        return self.form_class
    
host_new_listing_view = HostNewListingView.as_view()


class HostListingDetailView(DetailView):
    model = Listing
    context_object_name = 'listing'
    template_name = 'host/listing_detail.html'
    
    # def disptach(self, request, *args, **kwargs):
    #     request_host = self.request.user.host
    #     print(request_host)
    #     object_host = self.get_object.host
    #     print(object_host)
    #     if request_host != object_host:
    #         return redirect('404_not_found')
    #     return super(HostListingDetailView, self).dispatch(request,*args, **kwargs)
        
    # def get_object(self, queryset=None):
    #     obj = super(HostListingDetailView, self).get_object(queryset=queryset)
    #     if obj is None:
    #         raise Http404('Listing Profile is not found.')
    #     return obj
    
    # def get(self, request, *args, **kwargs):
    #     try:
    #         self.object = self.get_object()
    #     except Http404:
    #         raise Http404('Listing Profile is not found.')
    #     context = self.get_context_data(object=self.object)
    #     return self.render_to_response(context)
    
    # def get_queryset(self):
    #     listing_obj = self.model.objects.prefetch_related()
    #     return listing_obj
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['update_listing_form'] = ListingForm
        context['listing_images_form'] = ListingImageForm
        context['Bed_form'] = BedForm        
        return context

host_listing_detail_view = HostListingDetailView.as_view()


def host_listing_detail_view(request, pk):
    context = {}
    listing = Listing.objects.get(id=pk)
    context['listing'] = listing
    context['form'] = BedForm

    if request.method == 'POST':
        form = BedForm(request.POST,request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.listing = listing
            instance.host = request.user.host
            instance.save()
            return redirect('host_listing_lists_view')
    return render(request, 'host/host_listing_detail.html', context)


class HostListingListView(ListView):
    model = Listing
    template_name = 'host/host_listings.html'
    context_object_name = 'host_listings'
    
    def get_queryset(self):
        listings = Listing.objects.filter(host=self.request.user.host)
        return listings

host_listing_lists_view = HostListingListView.as_view()


class HostListingUpdateView(UpdateView):
    model = Listing
    form_class = ListingForm
    template_name = 'host/host_new_listing.html'
    
    def dispatch(self, request, *args, **kwargs):
        if self.get_object().host != self.request.user.host:
            return redirect('404_not_found')
        return super().dispatch(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if obj is None:
            raise Http404("Listing Object is Not Found.")
        return obj
    
    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            raise Http404("Listing object is not found.")
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
    
    def form_valid(self, form):
        self.object = form.save()
        #send_notification_email_host.delay()
        return self.get_success_url()

    def get_success_url(self):
        return redirect('host_listing_lists_view')
    
host_listing_update_view = HostListingUpdateView.as_view()


   
class HostBedCreateView(FormView):
    form_class = BedForm
    temaplate_name = 'new_Bed_creation.html'
    
    def form_valid(self, form):
        listing_bed = form.save(commit=False)
        listing_bed.host = self.request.user.host
        listing_bed.available_beds = listing_bed.number_of_beds
        listing_bed.save()
        #send_congrats_notification.delay()
        #send_admin_notification.delay()
        return redirect('listing_profile', pk=listing.id)
    
    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))
    
    def get_form_class(self):
        return self.form_class


host_bed_create_view = HostBedCreateView.as_view()


def host_add_new_bed(request, pk):
    context = {}
    try:
        listing = Listing.objects.get(id=pk)
    except Listing.DoesNotExist:
        pass

    context['listing'] = listing
    context['form'] = BedForm
    if request.method == 'POST':
        form = BedForm(request.POST,request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.listing = listing
            instance.host = request.user.host
            instance.available_beds=request.POST['number_of_beds']
            instance.save()
            return redirect('host_listing_lists_view')
    return render(request, 'host/host_add_new_bed.html', context)


class HostBedUpdateView(UpdateView):
    model = Bed
    form_class = BedForm
    template_name = 'host_Bed_update.html'
    
    def dispatch(self, request, *args, **kwargs):
        if self.get_object().host != self.request.user.host:
            return redirect('404_not_found')
        return super().dispatch(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if obj is None:
            raise Http404("Listing Object is Not Found.")
        return obj
    
    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            raise Http404("Listing object is not found.")
        context = self.get_context_data(object=sef.object)
        return self.render_to_response(context)
    
    def form_valid(self, form):
        self.object = form.save()
        #send_notification_email_host.delay()
        return super().form_valid(form)
    
    def get_success_url(self):
        return redirect('listing_profile', pk=self.object.id)


host_bed_update_view = HostBedUpdateView.as_view()


class HostBedDetailView(DetailView):
    model = Bed
    context_object_name = 'Bed'
    template_name = 'Bed_detail.html'
    
    def disptach(self, request, *args, **kwargs):
        request_host = self.request.user.host
        object_host = self.get_object.host
        if request_host != object_host:
            return redirect('404_not_found')
        return super(HostBedDetailView, self).dispatch(request,*args, **kwargs)
        
    def get_object(self, queryset=None):
        obj = super(HostBedDetailView, self).get_object(queryset=queryset)
        if obj is None:
            raise Http404('Listing Profile is not found.')
        return obj
    
    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            raise Http404('Listing Profile is not found.')
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
    
    def get_queryset(self):
        listing_obj = self.model.objects.all_with_prefetch_details()
        return listing_obj
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


host_bed_detail_view = HostBedDetailView.as_view()

class HostBedListView(ListView):
    model = Bed
    template_name = 'host_Beds_list.html'
    context_object_name = 'host_Beds'
    
    def get_queryset(self):
        listings = Bed.objects.filter(host=self.request.user.host)
        return listings
    
host_beds_list_view = HostBedListView.as_view()


@login_required
def stop_accepting_bookings(request, pk):
    try:
        listing = Listing.objects.get(id=pk)
    except Listing.DoesNotExist:
        pass
    if not request.user.host == listing.host:
        return redirect('host_error_view')
    listing.is_active = False
    listing.save()
    for bed in listing.beds.all():
        bed.close_bookings()
    return redirect('host_listing_lists_view')

@login_required
def start_accepting_bookings(request, pk):
    try:        
        listing = Listing.objects.get(id=pk)
    except Listing.DoesNotExist:
        pass
    if not request.user.host == listing.host:
        return redirect('host_error_view')

    listing.is_active = True
    listing.save()
    for bed in listing.beds.all():
        bed.open_bookings()

    return redirect('host_listing_lists_view')

@login_required
def host_update_timings(request, pk):
    try:
        listing = Listing.objects.get(pk=pk)
    except Listing.DoesNotExist:
        pass

    if not request.user.host == listing.host:
        return redirect('host_error_view')

    context = {}
    listing_timings = ListingBusinessHour.objects.get(listing=listing)
    context['form'] = ListingBusinessHourForm(instance=listing_timings)
    if request.method == 'POST':
        form = ListingBusinessHourForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.listing = listing
            instance.save()
            return redirect('host_listing_lists_view')
    return render(request, 'host/host_update_timings.html', context)

@login_required
def host_add_timings(request, pk):
    try:
        listing = Listing.objects.get(pk=pk)
    except Listing.DoesNotExist:
        pass

    if not request.user.host == listing.host:
        return redirect('host_error_view')

    context = {}
    context['form'] = ListingBusinessHourForm
    if request.method == 'POST':
        form = ListingBusinessHourForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.listing = listing
            instance.save()
            return redirect('host_listing_lists_view')
    return render(request, 'host/host_update_timings.html', context)    


from django.forms import modelformset_factory

@login_required
def host_add_galary_images(request, pk):
    context = {}
    listing=Listing.objects.get(pk=pk)
    ListingPhotoFormSet = modelformset_factory(ListingPhoto, form=ListingPhotoForm, extra=4)
    if request.method == 'POST':
        listing_form_set = ListingPhotoFormSet(request.POST, request.FILES, queryset=ListingPhoto.objects.none())

        if listing_form_set.is_valid():
            for form in listing_form_set.cleaned_data:
                if form:
                    image = form['image']
                    photo = ListingPhoto(listing=listing, image=image)
                    photo.save()
            return redirect('host_listing_detail_view', pk=listing.id)
        return render(request, 'host/host_add_galary_images.html', {'formset': listing_form_set})
    listing_form_set = ListingPhotoFormSet(queryset=ListingPhoto.objects.none())
    return render(request, 'host/host_add_galary_images.html', {'formset': listing_form_set})


@login_required(login_url='/host/login/')
def host_dashboard(request):
    context = {}
    listing = Listing.objects.get(host=request.user.host)
    context['upcoming_bookings'] = Booking.objects.filter(booking_listing=listing, is_checkin=False, is_cancel=False)
    return render(request, 'host/host_dashboard.html', context)


@login_required
def host_close_bed_bookings(request, pk):
    try:
        bed = Bed.objects.get(id=pk)
    except Bed.DoesNotExist:
        pass
    request_user=request.user.host
    if not bed.host == request_user:
        return redirect('host_error_view')

    bed.close_bookings()
    return redirect('host_listing_lists_view')


@login_required
def host_open_bed_bookings(request, pk):
    try:
        bed = Bed.objects.get(id=pk)
    except Bed.DoesNotExist:
        pass
    request_user = request.user.host
    if not bed.host == request_user:
        return redirect('host_error_view')

    bed.open_bookings()
    return redirect('host_listing_lists_view')


@login_required
def host_close_all_bookings(request):
    try:
        host = Host.objects.get(user=request.user)

    except Host.DoesNotExist:
        pass    
    listings = Listing.objects.filter(host=host)
    for listing in listings:
        listing.close_bookings()
    return redirect('host_listing_lists_view')


@login_required
def host_open_all_bookings(request):
    try:
        host = Host.objects.get(user=request.user)
    except Host.DoesNotExist:
        pass

    listings = Listing.objects.filter(host=host)
    for listing in listings:
        listing.open_bookings()
    return redirect('host_listing_lists_view')


@login_required
def host_current_bookings(request):
    context = {}
    listing = Listing.objects.get(host=request.user.host)
    bookings = Booking.objects.filter(booking_listing=listing, is_checkin=True,is_checkout=False)
    context['bookings'] = bookings
    return render(request, 'host/host_current_bookings.html', context)


@login_required
def host_past_bookings(request):
    context = {}
    listing = Listing.objects.get(host=request.user.host)
    bookings = Booking.objects.filter(booking_listing=listing, is_checkin=True, is_checkout=True)
    context['bookings'] = bookings
    return render(request, 'host/host_past_bookings.html', context)


@login_required
def host_accpect_guest_checkin(request, pk):
    context = {}

    try:
        booking = Booking.objects.get(id=pk)
    except Booking.DoesNotExist:
        pass
    
    context['booking'] = booking
    if booking.booking_listing.host == request.user.host:          
        if booking.is_cancel:
            context['status'] = 'Booking Already Cancled'
            return render(request, 'host/error_checkin.html', context)
        if booking.is_checkin:
            context['status'] = 'Booking Already Checkin'
            return render(request, 'host/error_checkin.html', context)
        if booking.is_checkout:
            context['status'] = 'Something Went Wrong'
            return render(request, 'host/error_checkin.html',context)
        booking.host_accept_checkin()
        return redirect('host_dashboard')
    else:
        return redirect('host_error_view')


@login_required
def host_checkout_guest(request, pk):
    context = {}
    try:
        booking = Booking.objects.get(id=pk)
    except Booking.DoesNotExist:
        pass

    context['booking'] = booking

    if booking.booking_listing.host == request.user.host:
        if booking.is_cancel:
            context['status'] = 'Booking Already Cancled'
            return render(request, 'host/error_checkin_checkout.html', context)
        if not booking.is_checkin:
            context['status'] = 'Guest doesnot checkin yet.'
            return render(request, 'host/error_checkin_checkout.html', context)
        if booking.is_checkout:
            context['status'] = 'Guest Already Checkout.'
            return render(request, 'host/error_checkin_checkout.html',context)
        booking.host_checkout()
        bed = Bed.objects.get(listing=booking.booking_listing, title=booking.booking_bed)
        bed.available_beds = bed.available_beds + 1
        bed.save()
        return redirect('host_dashboard')
    else:
        return redirect('host_error_view')


@login_required
def host_expire_booking(request, pk):
    try:
        booking = Booking.objects.get(id=pk)
    except Booking.DoesNotExist:
        pass

    context['booking'] = booking

    if booking.booking_listing.host == request.user.host:
        if booking.is_cancel:
            context['status'] = 'Booking data is not available.'
            return render(request, 'host/error_checkin_checkout.html', context)
        if booking.is_checkout:
            context['status'] = 'Guest Already Checkout.'
            return render(request, 'host/error_checkin_checkout.html', context)
        booking.cancel_booking_host()
        bed = Bed.objects.get(listing=booking.booking_listing, title=booking.booking_bed)
        bed.available_beds = bed.available_beds+1
        bed.save()
        return redirect('host_dashboard')
    else:
        return redirect('host_error_view')


@login_required
def host_profile(request):
    context = {}
    try:
        host = Host.objects.get(user=request.user)
    except Host.DoesNotExist:
        pass

    check_host_bank_details = HostBankDetail.objects.filter(host=request.user.host).exists()
    if check_host_bank_details:
        context['host_bank_details'] = True
    context['host_bank_details'] = False 
    context['host'] = host
    return render(request, 'host/host_profile.html', context)


host_bed_create_view = HostBedCreateView.as_view()


class HostUpdateProfile(UpdateView):
    model = Host
    form_class = HostUpdateForm
    template_name = 'host/host_update_profile.html'
    
    def dispatch(self, request, *args, **kwargs):
        if self.get_object().user != self.request.user:
            return redirect('404_not_found')
        return super().dispatch(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if obj is None:
            raise Http404("Listing Object is Not Found.")
        return obj
    
    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            raise Http404("Listing object is not found.")
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
    
    def form_valid(self, form):
        self.object = form.save()
        #send_notification_email_host.delay()
        return HttpResponseRedirect(reverse('host_profile'))
    
    def get_success_url(self):
        return redirect('host_profile')


host_update_profile = HostUpdateProfile.as_view()


@login_required
def add_host_bank_details(request):
    context = {}
    try:
        check_host_bank_details = HostBankDetail.objects.filter(host=request.user.host).exists()
    except HostBankDetail.DoesNotExist:
        pass

    if check_host_bank_details:
        return redirect('host_profile')
    context['form'] = HostBankForm
    if request.method == 'POST':
        form = HostBankForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.host=request.user.host
            instance.save()
            return redirect('host_profile')
    return render(request, 'host/add_host_bank_details.html', context)


def host_contact_us(request):
    return render(request, 'host/host_contact_us.html')
