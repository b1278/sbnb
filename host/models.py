from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


def update_last_login(sender, user, **kwargs):
    """
    """
    user.last_login = timezone.now()
    user_save(updated_fields=['last_login'])

    
def check_login_system(sender, host, **kwargs):
    pass


class TimeStamp(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    
    class Meta:
        abstract = True
     

class Area(models.Model):
    area_name = models.CharField(max_length=200)
    area_photo = models.ImageField(upload_to='images/areas', null=True, blank=True)

    def __str__(self):
        return self.area_name


class City(TimeStamp):
    city_name = models.CharField(max_length=200)
    city_photo = models.ImageField(upload_to='images/cities', null=True, blank=True)    
    
    def __str__(self):
        return self.city_name
    

class State(TimeStamp):
    state_name = models.CharField(max_length=150)
    state_photo = models.ImageField(upload_to='images/states', null=True, blank=True)

    def __str__(self):
        return self.state_name
    

class Country(TimeStamp):
    country_name = models.CharField(max_length=150)
    country_photo = models.ImageField(upload_to='images/countries', null=True, blank=True)

    def __str__(self):
        return self.country_name
    

class Address(models.Model):
    plat_number = models.CharField(max_length=100)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    postal_code = models.CharField(max_length=6)

    class Meta:
        abstract = True
        
        
class Host(TimeStamp, Address):
    
    Male = 'M'
    Female = 'F'
    Others = 'O'
    GENDERS = (
        (Male, 'Male'),
        (Female, 'Female'),
        (Others, 'Others'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    gender = models.CharField(max_length=1, choices=GENDERS)
    email_id = models.EmailField(max_length=200, unique=True)
    contact_number = models.CharField(max_length=15, unique=True)
    photo = models.ImageField(upload_to = 'hosts/images/', null=True, blank=True)
    is_active = models.BooleanField(default=False)
    land_mark = models.CharField(max_length=500,null=True, blank=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
    
    def get_full_n00e(self):
        return '{} {}'.format(self.first_name, self.last_name)
    
    def get_email(self):
        return self.email_id
    
    def send_welcome_email(self):
        pass
    
    def notificaiton_email(self):
        pass
    
    def update_active_status(self):
        self.is_active = False
        self.save(update_fields=['is_active'])
        

    
class HostBankDetail(TimeStamp):
    BANKS = (
        ('STATE_BANK_OF_INDIA', 'State Bank of India'),
        ('BANK_OF_BARODA', 'Bank of Baroda'),
        ('ICICI', 'ICICI'),
        ('HDFC', 'HDFC'),
        ('PUNJAB_NATIONAL_BANK', 'Punjab National Bank'),
    )
    host = models.OneToOneField(Host, on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=200, choices=BANKS)
    bank_account_number = models.CharField(max_length=25, unique=True)
    bank_account_holder = models.CharField(max_length=200)
    bank_ifsc_code = models.CharField(max_length=10)
    
    def __str__(self):
        return '{} {}'.format(str(self.host),self.bank_n00e)
    

class HostValet(TimeStamp):
    host = models.OneToOneField(Host, on_delete=models.CASCADE, related_name='host_valet')
    valet_amount = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return '{}'.format(str(self.host))


class HostValetTransaction(TimeStamp):
    CREDIT = 'C'
    DEBT = 'D'
    valet = models.ForeignKey(HostValet, on_delete=models.CASCADE, related_name='host_valet_transactions')
    transfered_amount = models.DecimalField(max_digits=10, decimal_places=2)
    available_amount = models.DecimalField(max_digits=10, decimal_places=2)
    transaction_type = models.CharField(max_length=5)

    def __str__(self):
        return '{}'.format(str(self.valet))

class Listing(TimeStamp, Address):

    Hotel = 'hotel'
    PG = 'pg'
    SHARED_HOUSE = 'sh'

    OPEN_FOR_GIRL = 'female'
    OPEN_FOR_BOY = 'male'
    OPEN_FOR_ALL = 'all'

    LISTING_TPYES = (
        (Hotel, 'Hotel'),
        (PG, 'Paying Guest'),
        (SHARED_HOUSE, 'Shared House')
    )

    LISTINGS_ECONOMIES = (
        ('Econamy', 'Econamy'),
        ('Business', 'Business'),
        ('General', 'General'),
    )
    OPEN_OPTIONS = (
        (OPEN_FOR_GIRL, 'Female'),
        (OPEN_FOR_BOY, 'Male'),
        (OPEN_FOR_ALL, 'Both'),
    )

    host = models.ForeignKey(Host, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    listing_type = models.CharField(max_length=25, choices=LISTING_TPYES)
    listing_econamy = models.CharField(max_length=25, choices=LISTINGS_ECONOMIES)
    listing_description = models.TextField(null=True, blank=True)
    details_description = models.TextField(null=True,blank=True)
    primary_contact_number = models.CharField(max_length=15)    
    secondary_contact_number = models.CharField(max_length=15)
    contact_email_id = models.EmailField(null=True, blank=True)
    website = models.CharField(max_length=200, null=True, blank=True)    
    is_active = models.BooleanField(default=True)

    number_of_beds = models.PositiveIntegerField()

    distance_to_center = models.DecimalField(max_digits=8, decimal_places=2,null=True, blank=True)
    is_smoking_allowed = models.BooleanField(default=False)
    is_pets_allowed = models.BooleanField(default=False)
    is_childrens_allowed = models.BooleanField(default=True)
    
    is_tv_available = models.BooleanField(default=True)
    is_gym_available = models.BooleanField(default=True)
    is_wardrob_available = models.BooleanField(default=True)
    is_laundry_available = models.BooleanField(default=True)
    is_parking_available = models.BooleanField(default=True)        
    is_restaurant_inside = models.BooleanField(default=False,)
    is_spa_available = models.BooleanField(default=False)
    is_elevator_available = models.BooleanField(default=False)
    is_wifi_available = models.BooleanField(default=False)

    
    banner_image = models.ImageField(upload_to='images/banner_images', null=True, blank=True)
    price_range = models.CharField(max_length=25, null=True, blank=True)
    open_for = models.CharField(max_length=10, null=True, blank=True, choices=OPEN_OPTIONS)

    def __str__(self):
        return self.title
    
    def update_passive_status(self):
        self.is_active = False
        self.save(update_fields=['is_active'])
    
    def update_active_status(self):
        self.is_active = True
        self.save(update_fields=['is_active'])
        
    

class ListingPrice(TimeStamp):
    BED = 'bed'
    BATH = 'bath'
    BED_AND_BATH = 'bed_and_bath'
    LOCKER = 'locker'
    BED_AND_LOCKER = 'bed_and_locker'
    ALL = 'all'

    SEGMENTATIONS = (
        (BED, 'Only Bed'),
        (BATH, 'Only Bath'),
        (LOCKER, 'Only Locker'),
        (BED_AND_BATH, 'Bed Plus BATH'),
        (BED_AND_LOCKER, 'Bed Plus Locker'),
        (ALL, 'All'),
    )

    segmentation = models.CharField(max_length=50, choices=SEGMENTATIONS)
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name='listing_prices')
    min_price = models.DecimalField(max_digits=10, decimal_places=2)
    max_price = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return str(self.listing)


class ListingBusinessHour(models.Model):
    
    ONE = '1'
    TWO = '2'
    THREE = '3'
    FOUR = '4'
    FIVE = '5'
    SIX = '6'
    SEVEN = '7'
    EIGHT = '8'
    NINE = '9'
    TEN = '10'
    ELEVEN = '11'
    TWELEVE = '12'
    THIRTEEEN = '13'
    FOURTEEN = '14'
    FIFTEEN = '15'
    SIXTEEN = '16'
    SEVENTEEN = '17'
    EIGHTEEN = '18'
    NINTEEN = '19'
    TWENTEY = '20'
    TWENTEY_ONE = '21'
    TWENTEY_TWO = '22'
    TWENTEY_THREE = '23'
    TWENTEY_FOUR = '24'

    BUSINESS_HOURS = (

        (ONE , '1:00'),
        (TWO ,'2:00'),
        (THREE , '3:00'),
        (FOUR , '4:00'),
        (FIVE , '5:00'),
        (SIX , '6:00'),
        (SEVEN , '7:00'),
        (EIGHT , '8:00'),
        (NINE , '9:00'),
        (TEN , '10:00'),
        (ELEVEN , '11:00'),
        (TWELEVE , '12:00'),
        (THIRTEEEN , '13:00'),
        (FOURTEEN , '14:00'),
        (FIFTEEN , '15:00'),
        (SIXTEEN , '16:00'),
        (SEVENTEEN , '17:00'),
        (EIGHTEEN , '18:00'),
        (NINTEEN , '19:00'),
        (TWENTEY , '20:00'),
        (TWENTEY_ONE , '21:00'),
        (TWENTEY_TWO , '22:00'),
        (TWENTEY_THREE , '23:00'),
        (TWENTEY_FOUR , '24:00'),

    )

    listing = models.OneToOneField(Listing, on_delete=models.CASCADE, related_name='listing_business_hours')
    monday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    monday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    
    tuesday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    tuesday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    
    wednesday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    wednesday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    
    thursday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    thursday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    
    friday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    friday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    
    saturday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    saturday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    
    sunday_opening = models.CharField(max_length=15, choices=BUSINESS_HOURS)
    sunday_closing = models.CharField(max_length=15, choices=BUSINESS_HOURS)    


from django.template.defaultfilters import slugify

def get_host_listing_photo_name(instance, filename):
    title = instance.listing.title
    slug = slugify(title)
    return 'host_lising_images/%s-%s' % (slug, filename)
class ListingPhoto(TimeStamp):
    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name='listing_photos')
    image = models.ImageField(upload_to=get_host_listing_photo_name, verbose_name='image')
    
    def __str__(self):
        return str(self.listing)
    

class Bed(TimeStamp):
    
    DELUX_BED = 'delux_BED'
    EXECUTIVE_BED = 'executive_BED'
    SUPER_DELUX_BED = 'super_delux_BED'
    SUPER_EXECUTIVE_BED = 'super_executive_BED'
    GRAND_MINI_SUITE_BED = 'grand_mini_suite_BED'
    F00ILY_BED = 'f00ly_BED'
    CLASSIC_FAMILY_BED = 'classic_family_BED'
    SINGLE_BED_BED = 'single_bed_BED'
    PRIVATE_BED = 'private_BED'
    DOUBLE_BED_BED = 'double_bed_BED'
    SHARED_BED = 'shared_BED'


    KING_SIZE_BED = 'king_size_bed'
    QUEEN_SIZE_BED = 'queen_size_bed'
    SINGLE_BED = 'single_bed'
    DOUBLE_BED  =  'double_bed'

    WESTERN_STYLE_TOLIET = 'western_style_toilet'
    INDIAN_STYLE_TOILET = 'indian_style_toilet'

    BED_TYPES = (
        ('AC', 'AC'),
        ('Non AC', 'Non AC')
    )

    BED_CATS = (

        (DELUX_BED ,'Delux Bed'),
        (EXECUTIVE_BED ,'Executive Bed'),
        (SUPER_DELUX_BED , 'Super Delux Bed'),
        (SUPER_EXECUTIVE_BED , 'Super Executive Bed'),
        (GRAND_MINI_SUITE_BED , 'Grand Mini Suite Bed'),
        (F00ILY_BED , 'Family Bed'),
        (CLASSIC_FAMILY_BED , 'Classic Family Bed'),
        (SINGLE_BED_BED , 'Single Bed'),
        (PRIVATE_BED , 'Private Bed'),
        (DOUBLE_BED_BED , 'Double Bed'),
        (SHARED_BED , 'Shared Bed'),
    
    )

    TOILET_TYPES = (
        (WESTERN_STYLE_TOLIET, 'Western Toilet Seat'),
        (INDIAN_STYLE_TOILET, 'Indian Toilet Seat'),
    )

    listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name='beds')
    host = models.ForeignKey(Host, on_delete=models.CASCADE)
    bed_type = models.CharField(max_length=50, choices=BED_TYPES)
    bed_category = models.CharField(max_length=25, choices=BED_CATS)
    title = models.CharField(max_length=150)
    description = models.TextField(null=True, blank=True)
    bed_price = models.DecimalField(max_digits=10, decimal_places=2)
    is_active = models.BooleanField(default=True)
    number_of_guest = models.PositiveIntegerField(null=True, blank=True)
    number_of_beds = models.PositiveIntegerField(null=True, blank=True)
    available_beds = models.PositiveIntegerField(null=True, blank=True)
    daily_house_keeping_available = models.BooleanField(default=True)
    bed_banner_image = models.ImageField(upload_to='images/listings/beds', null=True, blank=True)
    
    def __str__(self):
        return '{}-{}'.format(self.title, str(self.listing))
    
    def close_bookings(self):
        self.is_active=False
        self.save(update_fields=['is_active'])

    def open_bookings(self):
        self.is_active=True
        self.save(update_fields=['is_active'])
    
class BedPhoto(TimeStamp):
    bed = models.ForeignKey(Bed, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/listings/rooms/')



# class Bath(TimeStamp):
#     listing = models.ForeignKey(Listing, on_delete=models.CASCADE, related_name='baths')
#     bath_type = models.CharField(max_length=)