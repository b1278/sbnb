# Generated by Django 2.1.10 on 2019-09-07 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('host', '0019_bed_number_of_beds'),
    ]

    operations = [
        migrations.AddField(
            model_name='bed',
            name='available_beds',
            field=models.PositiveIntegerField(blank=True, null=True),
        ),
    ]
