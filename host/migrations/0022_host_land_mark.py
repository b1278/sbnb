# Generated by Django 2.1.10 on 2019-09-11 09:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('host', '0021_auto_20190910_0410'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='land_mark',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
