# Generated by Django 2.1.10 on 2019-08-29 01:25

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('host', '0013_auto_20190828_0741'),
    ]

    operations = [
        migrations.CreateModel(
            name='ListingBusinessHour',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('monday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('monday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('tuesday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('tuesday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('wednesday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('wednesday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('thursday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('thursday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('friday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('friday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('saturday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('saturday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('sunday_opening', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('sunday_closing', models.CharField(choices=[('1', '1:00AM'), ('2', '2:AM'), ('3', '3:AM'), ('4', '4:AM'), ('5', '5:AM'), ('6', '6:AM'), ('7', '7:AM'), ('8', '8:AM'), ('9', '9:AM'), ('10', '10:AM'), ('11', '11:AM'), ('12', '12:PM'), ('13', '13:PM'), ('14', '14:AM'), ('15', '15:PM'), ('16', '16:PM'), ('17', '17:PM'), ('18', '18:PM'), ('19', '19:PM'), ('20', '20:PM'), ('21', '21:PM'), ('22', '22:PM'), ('23', '23:PM'), ('24', '24:PM')], max_length=15)),
                ('listing', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='listing_business_hours', to='host.Listing')),
            ],
        ),
        migrations.CreateModel(
            name='ListingPrice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('segmentation', models.CharField(choices=[('bed', 'Bed'), ('bath', 'Bath'), ('locker', 'Locker'), ('bed_and_bath', 'Bed Plus BATH'), ('bed_and_locker', 'Bed Plus Locker'), ('all', 'All')], max_length=50)),
                ('min_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('max_price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('listing', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='listing_prices', to='host.Listing')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='room',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
    ]
