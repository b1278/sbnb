from django.conf.urls import url
from django.contrib.auth import views as auth_views
from host import views


urlpatterns = [
    
    url('registration/', views.host_creation_view, name='host_creation_view'),
    url('host_thank_you_view/', views.host_thank_you_view, name='host_thank_you_view'),
    url('login/', views.host_login_view, name='host_login_view'),
    url('logout/', views.host_logout_view, name='host_logout_view'),    
    url('my-listings/add-new-listing/', views.host_new_listing_view, name='host_new_listing_view'),
    url('my-listings/(?P<pk>[0-9]+)/$', views.host_listing_detail_view, name='host_listing_detail_view'),
    url('my-listings/$', views.host_listing_lists_view, name='host_listing_lists_view'),
    url('update/my-listing/(?P<pk>[0-9]+)/$', views.host_listing_update_view, name='host_listing_update_view'),
    url('my-listings/(?P<pk>[0-9]+)/add_new_bed/$', views.host_add_new_bed, name='host_add_new_bed'),
    url('my-listings/update_bed/$', views.host_bed_update_view, name='host_bed_update_view'),
    url('mylistings/bed/<int:pk>/$', views.host_bed_detail_view, name='host_bed_detail_view'),
    url('my-listings/beds/$', views.host_beds_list_view, name='host_beds_list_view'),
    url('my-dashboard/$', views.host_dashboard, name='host_dashboard'),
    url('my-dashboard/live_bookings/$', views.host_current_bookings, name='host_current_bookings'),
    url('my-listings/(?P<pk>[0-9]+)/stop_accepting_bookings/', views.stop_accepting_bookings, name='stop_accepting_bookings'),
    url('my-listings/(?P<pk>[0-9]+)/start_accepting_bookings/', views.start_accepting_bookings, name='start_accepting_bookings'),
    url('my-listings/(?P<pk>[0-9]+)/host_add_galary_images/', views.host_add_galary_images, name='host_add_galary_images'),
    url('my_profile/(?P<pk>[0-9]+)/update/', views.host_update_profile, name='host_update_profile'),
    url('my_profile/', views.host_profile, name='host_profile'),
    url('my_bank_details', views.add_host_bank_details, name='add_host_bank_details'),
    url('contact-us', views.host_contact_us, name='host_contact_us'),


    url('host_accpect_guest_checkin/(?P<pk>[0-9]+)/done', views.host_accpect_guest_checkin, name='host_accpect_guest_checkin'),
    url('host_checkout_guest/(?P<pk>[0-9]+)/done', views.host_checkout_guest, name='host_checkout_guest'),
    url('host_past_bookings/', views.host_past_bookings, name='host_past_bookings'),

    url('host_close_bed_bookings/(?P<pk>[0-9]+)/done', views.host_close_bed_bookings, name='host_close_bed_bookings'),
    url('host_open_bed_bookings/(?P<pk>[0-9]+)/done', views.host_open_bed_bookings, name='host_open_bed_bookings'),


    url('host_add_timings/(?P<pk>[0-9]+)/done', views.host_add_timings, name='host_add_timings'),
    url('host_update_timings/(?P<pk>[0-9]+)/done', views.host_update_timings, name='host_update_timings'),

]

