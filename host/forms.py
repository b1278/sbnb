
import unicodedata

from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, 
)

from django.contrib.auth.hashers import (
    UNUSABLE_PASSWORD_PREFIX, identify_hasher,
)
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.text import capfirst
from django.utils.translation import gettext, gettext_lazy as _
from django import forms
from django.utils.html import strip_tags
from django.utils.translation import gettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column
from host.models import Host,HostBankDetail,Listing,ListingPrice,ListingBusinessHour,Bed,ListingPhoto,BedPhoto


user_model = get_user_model()


class UsernameField(forms.CharField):
    def to_python(self, value):
        return unicodedata.normalize('NFKC', super().to_python(value))
    
    def widget_attr(self, widget):
        attrs = super().widget_attrs(widget)
        attrs['autocapitalize'] = 'None'
        return attrs
    
    
class HostLoginForm(forms.Form):
    """
    Host Login Form.
    """
    username =UsernameField(
        widget=forms.TextInput(
            attrs={
                'autocomplte':'username', 
                'autofocus': True
            }
        )
    )
    password = forms.CharField(
        label=_("Password")
    )
    error_messages = {
        'invalid_login': _(
            'Please Enter Valid Credentials.'    
        ),
        'inactive': _("Due to some reasons Account is in Hold.")
    }
    
    def __init__(self, request=None, *args,**kwargs):
        self.request = request
        self.user_cache = None
        super().__init__(*args, **kwargs)
        self.username_field = user_model._meta.get_field(user_model.USERNAME_FIELD)
        self.fields['username'].max_length = self.username_field.max_length or 254
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)
        
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        
        if username is not None and password:
            self.user_cache = authenticate(self.request, username=username, password=password)
            if self.user_cache is None:
                raise self.get_invalid_login_error()
            else:
                self.confirm_login_allowed(self.user_cache)
                
        return self.cleaned_data
    
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )
    
    def get_user(self):
        return self.user_cache
    
    def get_invalid_login_error(self):
        return forms.ValidationError(
            self.error_messages['invalid_login'],
            code='invalid_login',
            params={'username': self.username_field.verbose_name},
        )
    
class HostCreationForm(forms.ModelForm):
    error_messages = {
        
        'invalid_email_id': _("Please Enter Valid Email address."),
        'invalid_contact_number': _("Please enter Valid Contact Number."),
        'password_small_letter': _("Password contains Atleast One Small Letter."),
        'password_capital_letter': _("Password contains Atleat One Capital Letter."),
        'password_number': _("Password contains atleast one number"),
        'invalid_password': _("Password and Confirm Password Dosn't match"),
        'invalid_pwd_length':  _("Password Should be 8 letters Length."),
        'invalid_username': _("Username is not available.Please choose another")
        
    }
    user_name = forms.CharField()
    password1 = forms.CharField(
        label=_("Password"),
    )
    password2 = forms.CharField(
        label=_("Confirm Password"),
    )
    class Meta:
        model = Host
        fields = (
            'first_name', 
            'last_name', 
            'gender',
            'email_id', 
            'contact_number', 
            'photo', 
            'plat_number', 
            'area', 
            'city', 
            'state', 
            'country',
            'postal_code',
            'land_mark',
         )
        # field_classes = {'username': UsernameField}
        
        labels = {
            'first_name': _("First Name"), 
            'last_name': _("Last Name"),
            'gender': _("Gender"),
            'email_id': _("Email Address"), 
            'contact_number': _("Contact Number"),
            'photo': _("Photo"),
            'plat_number': _("Plat/Suit Number"),
            'area': _("Area"),
            'city': _("City"),
            'state': _("State"), 
            'country': _("Country"),
            'postal_code': _("Pincode"),
            'land_mark': _("Land Mark")        
        }
    
    
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['invalid_password'],
                code='invalid_password',
            )
        return password2
    
    def clean_password(self):
        password = self.cleaned_data.get('password1')
        if password1:
            if len(password1) <8:
                raise forms.ValidationError(
                    self.error_messages['invalid_pwd_length'],
                    code='invalid_pwd_length',
                )
            
    def _post_clean(self):
        super()._post_clean()
        password = self.cleaned_data.get('password2')
        if password:
            try:
                password_validation.validate_password(password, self.instance)
            except forms.ValidationError as error:
                self.add_error('password2', error)
                
    def clean_email_id(self):
        email = self.cleaned_data.get('email_id')
        if email:
            email_check = Host.objects.filter(email_id=email).exists()
            if email_check:
                raise forms.ValidationError(
                    self.error_messages['invalid_email'],
                    code='invalid_email',
                )
        return email
    
    def clean_contact_number(self):
        number = self.cleaned_data.get('contact_number')
        if number:
            number_check = Host.objects.filter(contact_number=number).exists()
            if number_check:
                raise forms.ValidationError(
                    self.error_messages['invalid_contact_number'],
                    code='invalid_contact_number',
                )
        return number

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if username:
            username_check = user_model.objects.filter(username=username).exists()
            if username_check:
                raise forms.ValidationError(
                    self.error_messages['invalid_username'],
                    code='invalid_username'
                )
        return username
                
    def save(self,commit=True):
        super().save(commit=False)
        if commit:
            username=self.cleaned_data.get('user_name')
            user=user_model.objects.create(
                username=username,
            )
            user.set_password(self.cleaned_data.get('password1'))
            user.save()
            host = Host.objects.create(
                user=user,
                first_name=self.cleaned_data.get('first_name'),
                last_name=self.cleaned_data.get('last_name'),
                email_id=self.cleaned_data.get('email_id'),
                contact_number=self.cleaned_data.get('contact_number'),
                gender=self.cleaned_data.get('gender'),
                photo=self.cleaned_data.get('photo'),
                plat_number=self.cleaned_data.get('plat_number'), 
                area=self.cleaned_data.get('area'),
                city=self.cleaned_data.get('city'), 
                state=self.cleaned_data.get('state'), 
                country=self.cleaned_data.get('country'),
                postal_code=self.cleaned_data.get('postal_code'),
            )
            host.save()
            #sent_welcome_email_to_host.delay(self.cleaned_data['email_id])
            #send_admin_notification.delay
        return host
    
    
class ListingForm(forms.ModelForm):
    
    error_messages = {
        'invalid_primary_contact': _("Number already taken please Add other.")
    }

    segmentation = forms.ChoiceField(choices=ListingPrice.SEGMENTATIONS, widget=forms.Select())
    # monday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # tuesday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # wednesday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # thursday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # friday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # saturday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # sunday_opening = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # monday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # tuesday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # wednesday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # thursday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # friday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # saturday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())
    # sunday_closing = forms.ChoiceField(choices=ListingBusinessHour.BUSINESS_HOURS,widget=forms.Select())      
    min_price = forms.DecimalField(max_digits=10, decimal_places=2)
    max_price = forms.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        model = Listing
        exclude = ('host', 'is_active')
        
        labels = {
            'title': _("Title"),
            'listing_type': _("Listing Type"),
            'listing_econamy': _("Listing Class"),
            'description': _("Description"),
            'primary_contact_number': _("Primary Contact Number"),
            'secondary_contact_number': _("Secondary Contact Number"),
            'number_of_bedBeds': _("Number of BedBeds"),
            'number_of_beds': _("Number od Beds"),
            'is_smoing_allowed': _("Smoke Allowed"),
            'is_pets_allowed': _("Pets Allowed"),
            'is_childrens_allowed': _("Childrens Allowed"),
            'is_tv_available' : _("TV Aailable "),
            'is_gym_available' : _("Gym Available"),
            'is_wardrob_available' : _("Wardrob Available"),
            'is_laundry_available' : _("Laundry Available"),
            'is_parking_available' : _("Parking Available"),           
            # 'monday_opening': _(" "),
            # 'monday_closing': _(''),
            # 'tuesday_closing': _(''),
            # 'tuesday_opening': _(''),
            # 'wednesday_opening': _(''),
            # 'wednesday_closing': _(''),
            # 'thursday_opening': _(''),
            # 'thursday_closing': _(''),
            # 'friday_opening': _(''),
            # 'friday_closing': _(''),
            # 'saturday_opening': _(''),
            # 'saturday_closing': _(''),
            # 'sunday_opening': _(''),
            # 'sunday_closing': _(''),
        }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['is_wifi_available']=forms.BooleanField(
            label=_("Wifi Available"),
            required=False,
            initial=True
        )    

    def clean(self):
            super().clean()
            strip_tags(self.cleaned_data['title'])
            strip_tags(self.cleaned_data['listing_description'])
            return self.cleaned_data
        
    # def clean_primary_contact_number(self):
    #         pmry_contact = self.cleaned_data.get('primary_contact_number')
    #         if pmry_contact:
    #             check = Host.objects.filter(contact_number=pmry_contact).exists()
    #             if check:
    #                 raise forms.ValidationError(
    #                     self.error_messages['invalid_primary_contacy'],
    #                     code='invalid_primary_contact'
    #                 )
        

class ListingPhotoForm(forms.ModelForm):
    class Meta:
        model = ListingPhoto
        fields = ('image', )
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].widget.attrs.update({
            'class':'file-upload-field'
        })

class ListingBusinessHourForm(forms.ModelForm):
    class Meta:
        model = ListingBusinessHour
        exclude = ('listing', )

        
class BedForm(forms.ModelForm):
    class Meta:
        model = Bed
        exclude = ('host', 'listing', 'is_active', 'available_beds',)
        
        labels = {
            'Bed_type': _("Bed Type"),
            'Bed_category': _("Bed Category"),
            'title': _("Title"),
            'description':_("Description"),
            'Bed_price': _("Bed Price"),
        }
        
        def clean(self):
            super().clean()
            strip_tags(self.cleaned_data['title'])
            strip_tags(self.cleaned_data['description'])
     
    
class BedPhotoForm(forms.ModelForm):
    class Meta:
        model = BedPhoto
        fields = ('image', )


class HostUpdateForm(forms.ModelForm):
    class Meta:
        model = Host
        exclude = ('user', )


class HostBankForm(forms.ModelForm):
    class Meta:
        model = HostBankDetail
        fields = ('bank_name', 'bank_account_number','bank_account_holder', 'bank_ifsc_code')
        labels = {
            'bank_name': 'Bank Name',
            'bank_account_holder': 'Account Holder',
            'bank_account_number': 'Account Number',
            'bank_ifsc_code': 'ifsc code'
        }