from django.conf.urls import url
from guest import views
from django.contrib.auth import views as auth_views
# from guest.payment_views import payment,response

urlpatterns = [
    
   
    url(r'^paytm/payment/$', views.paytm_payment, name='paytm_payment'),
    url(r'^payment-response/$', views.paytm_response, name='paytm_response'),
    url(r'^guest_pay_at_host/$', views.guest_pay_at_host, name='guest_pay_at_host'),
    url('^listings/(?P<pk>[0-9]+)/details', views.guest_listing_detail, name='guest_listing_detail'),
    url('^listings/area/(?P<pk>[0-9]+)/details', views.guest_area_listings, name='guest_area_listings'),
    url('^listings/', views.guest_search_listings_view, name='guest_search_listings_view'),
    # url('^listings/', views.listings_view, name='listings_view'),
    url('login/', views.guest_login_view, name='guest_login_view'),
    url('signup/', views.guest_signup_view, name='guest_signup_view'),
    url('logout/', views.guest_logout_view, name='guest_logout_view'),
    # url('payment/success/', views.guest_payment_success, name='guest_payment_success_view'),
    # url('payment/failure/', views.guest_payment_failure, name='guest_payment_failure_view'),      
    # url('payment/', views.guest_payment_view, name='guest_payment_view'),  
    url('^my-bookings/(?P<pk>[0-9]+)/cancel/', views.guest_cancel_booking,name='guest_cancel_booking'),
    url('^my-bookings/$', views.guest_bookings,name='guest_bookings'),


    url('^$', views.guest_listings_home, name='guest_listings_home'),


    # url(r'^home/',views.Home),
    # url(r'^Success/',views.success),
    # url(r'^Failure/',views.failure),
       
]

