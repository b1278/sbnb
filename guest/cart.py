from decimal import Decimal
from host.models import Listing


class Cart(object):
	def __init__(self, request):
		self.session = request.session
		cart = self.session.get(settings.CART_ID)
		if not cart:
			cart = self.session[settings.CART_ID]
		self.cart = cart


	def __iter__(self):
		listing_id = self.cart.keys()
		listings = Listing.objects.filter(id__in=listing_id)
		cart = self.cart.copy()

		for listing in listings:
			cart[str(listing.id)]['listing'] = listing

		for item in cart.values():
			yield item


	def __len__(self):
		pass

	def add_listing(self, listing, guests=1, price, update_guests=False, rooms=1, update_rooms=False):
		listing_id = str(listing.id)
		if listing_id not in self.cart:
			self.cart['listing_id'] = {'guests': 0, 'price': price}

		if update_guests:
			self.cart['listing_id']['guests'] = guests

		if update_rooms:
			self.cart['listing_id']['rooms'] = rooms

		else:
			self.cart['listing_id']['guests'] += guests
			self.cart['listing_id']['rooms'] += rooms
		self.save()

	def save(self):
		self.session.modified = True


	def remove(self, listing):
		listing_id = str(listing.id)
		if listing_id in self.cart:
			del self.cart[listing_id]
			self.save()

	def get_total_price(self):
		return sum(Decimal(item['price'])*item['rooms'] for item in self.cart.values())

	def clear(self):
		del self.session[settings.CART_ID]