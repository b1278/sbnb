import random
import string
import datetime
import pytz
from django.db import models
from django.contrib.auth.models import User
from host.models import Listing,Bed


class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	class Meta:
		abstract = True

class Guest(TimeStamp):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	first_name = models.CharField(max_length=150)
	last_name = models.CharField(max_length=150)
	email_id = models.EmailField(max_length=200)
	contact_number = models.CharField(max_length=250)
	profile_photo = models.ImageField(upload_to='guests/profile_photos', null=True, blank=True)
	
	def __str__(self):
		return "{} {}".format(self.first_name,self.last_name)
	
	@property
	def get_full_name(self):
		return self.first_name+self.last_name

	def send_welcome_email(self):
		pass


class Booking(TimeStamp):
	P_SOYO = 'soyo'
	P_HOST = 'host'
	primary_person = models.ForeignKey(User, on_delete=models.CASCADE)
	booking_date = models.DateField()
	booking_from = models.TimeField()
	booking_to = models.TimeField()
	number_of_guests = models.PositiveIntegerField()
	booking_id_num = models.CharField(max_length=15)
	is_amount_paid = models.BooleanField(default=False)
	total_amount = models.DecimalField(max_digits=10, decimal_places=2)
	booking_bed = models.CharField(max_length=255)
	booking_listing = models.ForeignKey(Listing, on_delete=models.CASCADE)
	is_checkin = models.BooleanField(default=False)
	is_checkout = models.BooleanField(default=False)
	is_cancel = models.BooleanField(default=False)
	guest_cancled = models.BooleanField(default=False)
	guest_cancled_at = models.DateTimeField(null=True, blank=True)
	host_cancled_at = models.DateTimeField(null=True, blank=True)
	host_cancled = models.BooleanField(default=False)
	payment_type = models.CharField(max_length=10, null=True, blank=True)
	is_guest_reviewed = models.BooleanField(default=False)

	def __str__(self):
		return '{} booked {}'.format(str(self.primary_person), str(self.booking_listing))

	def cancle_booking_guest(self):
		self.is_cancel = True
		self.guest_cancled = True
		self.save(update_fields=['is_cancel', 'guest_cancled'])

	def cancel_booking_host(self):
		self.host_cancled = True
		tx = pytz.timezone('Asia/Kolkata')
		self.host_cancled_at = datetime.datetime.now(tz)
		self.save(update_fields=['host_cancled', 'host_cancled_at'])
		
	def get_booking_id(self):
		sl = 8
		letters=string.ascii_lowercase
		s = ''.join(random.choice(letters) for x in range(sl))
		return s

	def save(self, *args, **kwargs):
		super().save(*args, **kwargs)
		self.booking_id = self.get_booking_id()

	def host_accept_checkin(self):
		self.is_checkin = True
		self.is_amount_paid = True
		self.save(update_fields=['is_checkin', 'is_amount_paid'])

	def host_checkout(self):
		self.is_checkout = True
		self.save(update_fields=['is_checkout'])

	@property
	def get_total_hours(self):
		return self.booking_to - self.booking_from

	@property
	def get_booking_hours_human_format(self):
		return '{} to {}'.format(self.booking_from, self.booking_to)


	def send_confirmation_email_to_guest(self):
		pass

	def send_confirmation_email_to_host(self):
		pass

	def send_invoice_to_guest(self):
		pass

	def send_guest_reminder(self):
		pass

	def send_feedback_form_request(self):
		pass


class Rating(TimeStamp):
	listing = models.ForeignKey(Listing, on_delete=models.CASCADE,related_name='ratings')
	cleanness_rating = models.PositiveIntegerField(null=True,blank=True)
	service_rating = models.PositiveIntegerField(null=True,blank=True)
	staff_rating = models.PositiveIntegerField(null=True,blank=True)
	total_avg_rating = models.PositiveIntegerField(null=True, blank=True)
	guest = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return '{}-{}'.format(str(self.listing), str(self.guest))


from django.conf import settings
from django.utils import timezone

class Payment(TimeStamp):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='payments', on_delete=models.CASCADE)
	ORDERID = models.CharField('ORDER ID', max_length=50)
	TXNDATE = models.DateTimeField('TXN DATE', default=timezone.now)
	TXNID = models.IntegerField('TXN ID')
	BANKTXNID = models.IntegerField('BANK TXN ID', null=True, blank=True)
	BANKNAME = models.CharField('BANK NAME', max_length=100, null=True, blank=True)
	RESPCODE = models.IntegerField('RESP CODE')
	PAYMENTMODE = models.CharField('PAYMENT MODE', max_length=25, null=True, blank=True)
	CURRENCY = models.CharField('CURRENCY', max_length=4, null=True, blank=True)
	GATEWAYNAME = models.CharField('GATEWAY NAME', max_length=50, null=True, blank=True)
	MID = models.CharField(max_length=50)
	RESPMSG = models.TextField('RESP MSG', max_length=250)
	TXNAMOUNT = models.FloatField('TXN AMOUNT')
	STATUS =  models.CharField('STATUS', max_length=25)

	def __str__(self):
		return '{}-{}'.format(str(self.user), self.STATUS)	
