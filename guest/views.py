from django.shortcuts import render
from django.urls import reverse_lazy
from host.models import Listing,Area,Bed
from django.db.models import Sum
from django.views.generic import ListView, FormView,View,TemplateView
import logging
from urllib.parse import urlparse, urlunparse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, QueryDict,Http404
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect,reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)
from django.shortcuts import render, render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template.loader import get_template
from django.template import Context, Template,RequestContext
from django.contrib import messages
import logging, traceback
import guest.constants as constants
import guest.config as config
import hashlib
import requests
from random import randint
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import resolve_url
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth.views import LoginView,LogoutView
# from guest.filters import ListingsFilterSet
from guest.forms import BookingForm,GuestSignupForm,SearchForm,RatingForm
from guest.models import Guest,Booking,Rating,Payment



def guest_listings_home(request):
	context = {}
	listings = Listing.objects.filter(is_active=True)[:6]
	areas = Area.objects.all()[:7]
	listing_areas = Listing.objects.filter(is_active=True).values('area').annotate(count=Sum('area'))
	if request.user.is_authenticated:
		upcoming_bookings = Booking.objects.filter(primary_person=request.user, is_checkin=False, is_cancel=False).exists()
		if upcoming_bookings:
			upcoming_bookings = Booking.objects.filter(primary_person=request.user, is_checkin=False, is_cancel=False)
			context['upcoming_bookings'] = upcoming_bookings
			context['is_upcoming_bookings'] = True
		else:
			context['is_upcoming_bookings'] = False
			
	context['listings'] = listings	
	context['areas'] = areas
	context['search_url'] = reverse_lazy('guest_search_listings_view')
	context['search_form'] = SearchForm
	# context['most_populor_hotels'] = listings[:2]
	return render(request, 'guest/landing_index.html', context)


def guest_search_listings_view(request):
	print("im ahere")
	context = {}

	search_area = request.GET.get('search_area')
	search_type = request.GET.get('search_type')
	sample = request.GET.get('accommodation')
	gender = request.GET.get('gender')

	checkin_date = request.GET.get('checkin_date')
	checkin_time = request.GET.get('checkin_time')
	check_out_time = request.GET.get('check_out_time')

	listings = Listing.objects.all()
	# if search_area:
	# 	listings = listings.filter(area__area_name__icontains=search_area)

	# if search_type:
	# 	listings = listings.filter(listing_type__icontains=search_area)

	# if gender:
	# 	listings =  listings.filter(open_for=gender)
	context['listings'] = listings
	context['checkin_date'] = checkin_date
	return render(request, 'guest/listings.html', context)


def guest_area_listings(request, pk):
	try:
		area = Area.objects.get(pk=pk)
	except Area.DoesNotExist:
		pass

	context = {}
	context['area'] = area
	listings = Listing.objects.filter(area=area)
	context['listings'] = listings
	context['listing_area'] = area.area_name
	return render(request, 'guest/listings.html', context)


def guest_city_listings(request, pk):
	try:
		city = models.City.objects.get(pk=pk)
	except models.City.DoesNotExist:
		pass

	context = {}
	context['city'] = city
	listings = models.Listing.objects.filter(city=city)
	context['listings'] = listings
	return render(request, 'guest/listings.html', context)

@never_cache
def guest_listing_detail(request, pk):
	context = {}
	try:
		listing = Listing.objects.get(id=pk)
	except Listing.DoesNotExist:
		pass
	context['listing'] = listing
	beds = Bed.objects.filter(listing=listing, available_beds__gt=0)
	context['form'] = BookingForm
	context['beds'] = beds
	context['similar_listings'] = Listing.objects.filter(is_active=True).exclude(id=listing.id)[:4]
	if request.method == 'POST':
		form = BookingForm(request.POST)
		booking_date = request.POST['booking_date']
		booking_from = request.POST['booking_from']
		booking_to = request.POST['booking_to']
		# print(request.POST['booking_to'])
		import datetime
		booking_date = datetime.datetime.strptime(booking_date, '%Y-%m-%d').date()
		booking_from = datetime.datetime.strptime(booking_from, '%H:%M').time()
		booking_to = datetime.datetime.strptime(booking_to, '%H:%M').time()
		from datetime import date
		import pytz
		today = date.today()
		ind_timezone = pytz.timezone('Asia/Kolkata')
		current_time = datetime.datetime.now(ind_timezone)
		current_time = current_time.strftime('%H:%M')
		current_time = datetime.datetime.strptime(current_time, '%H:%M').time()
		print(current_time)
		print(booking_from)
		if booking_date < today:
			context['error_date'] = True
			return render(request, 'guest/guest_listing_detail.html', context)

		if booking_from >= booking_to:
			context['error_time'] = True
			return render(request, 'guest/guest_listing_detail.html', context)

		booking_check = Booking.objects.filter(booking_listing=listing,
												booking_from=request.POST['booking_from'], 
												booking_to=request.POST['booking_to'],
												booking_date=request.POST['booking_date']
												).exists()
		if booking_check:
			context['not_available'] = True
			return render(request, 'guest/guest_listing_detail.html', context)

		if form.is_valid():
			data = form
			guest = Guest.objects.get(email_id=request.user.username)
			booking_data = {}
			booking_data['listing'] = listing.title
			booking_data['primary_person'] = str(guest)
			booking_data['primary_person_email_id'] = guest.email_id
			booking_data['primary_person_phone'] = guest.contact_number
			booking_data['booking_bed'] = data.cleaned_data['booking_bed']
			booking_data['booking_date'] = str(data.cleaned_data['booking_date'])
			booking_data['checkin_time'] = str(data.cleaned_data['booking_from'])
			booking_data['checkout_time'] = str(data.cleaned_data['booking_to'])
			booking_data['total_guests'] = data.cleaned_data['number_of_guests']
			bed = Bed.objects.get(listing=listing, title=data.cleaned_data['booking_bed'])
			# total_price = bed.bed_price * data.number_of_beds
			# booking_data['total_price'] = total_price
			FMT = '%H:%M:%S'
			from datetime import datetime
			s1=str(data.cleaned_data['booking_from'])
			s2=str(data.cleaned_data['booking_to'])
			td = datetime.strptime(s2, FMT)-datetime.strptime(s1, FMT)
			days, hours, minutes = td.days, td.seconds // 3600, td.seconds // 60 % 60
			if minutes >= 1:
				hours = hours+1
				price = hours*bed.bed_price
				print(price)
			else:
				price = hours*bed.bed_price
				# print(price)
			booking_data['total_price'] = str(price)
			request.session['booking_data'] = booking_data
			# print("redirect")
			# print(request.session['booking_data'].keys())
			# for i in request.session['booking_data'].items():
				# print(i[1])
			# print('redirect')
			return redirect('paytm_payment')			
	return render(request, 'guest/guest_listing_detail.html',context)


# @login_required
# def guest_payment_view(request):
# 	print("hi")
# 	print('helllo')
# 	if request.session['booking_data']:
# 		print("hiii")
# 		context = {}
# 		txnid = get_transaction_id()
# 		hash_ = generate_hash(request, txnid)
# 		hash_string = get_hash_string(request, txnid)
# 		# context['action'] = 'https://sandboxsecure.payu.in/_payment'
# 		context['action'] = reverse_lazy('guest_payment_success_view')
# 		context['amount'] = float(request.session['booking_data']['total_price'])
# 		context['productinfo'] = request.session['booking_data']['listing']
# 		context['key'] = config.KEY
# 		context['txnid'] = txnid
# 		context['hash'] = hash_
# 		context['hash_string'] = hash_string
# 		context['first_name'] = request.session['booking_data']['primary_person']
# 		context['email'] = request.session['booking_data']['primary_person_email_id']
# 		context['phone'] = request.session['booking_data']['primary_person_phone']
# 		context['service_provider'] = constants.SERVICE_PROVIDER
# 		context['furl'] = reverse_lazy('guest_payment_failure_view')
# 		context['surl'] = reverse_lazy('guest_payment_success_view')
# 		context['booking_data'] = request.session['booking_data']
# 		return render(request, 'guest/payment.html', context)		

# 	else:
# 		return redirect('guest_listings_home')


# @login_required
# @csrf_exempt
# def guest_payment_success(request):
# 	booking_data = request.session['booking_data']
# 	booking_guest = Guest.objects.get(email_id=booking_data['primary_person_email_id'])
# 	booking_listing = Listing.objects.get(title=booking_data['listing'])
# 	booking_bed=Bed.objects.get(listing=booking_listing, title=booking_data['booking_bed'])
# 	booking = Booking.objects.create(
# 		primary_person=request.user,
# 		booking_date=booking_data['booking_date'],
# 		booking_from=booking_data['checkin_time'],
# 		booking_to=booking_data['checkout_time'],
# 		number_of_guests=booking_data['total_guests'],
# 		is_amount_paid=True,
# 		total_amount=booking_data['total_price'],
# 		booking_bed=booking_data['booking_bed'],
# 		booking_listing=booking_listing,
# 	)
# 	del request.session['booking_data']
# 	booking_bed.available_beds = booking_bed.available_beds-1
# 	booking_bed.save()
# 	request.session.modified = True
# 	context = {}
# 	context['booking_id'] = booking.booking_id
# 	return render(request, 'guest/guest_payment_success.html', context)


@login_required
def guest_cancel_booking(request, pk):
	try:
		booking = Booking.objects.get(primary_person=request.user, is_checkin=False, is_cancel=False)
	except Booking.DoesNotExist:
		return redirect('404_handler')

	booking.cancle_booking_guest()
	booking_bed = Bed.objects.get(listing=booking.booking_listing, title=booking.booking_bed)
	booking_bed.available_beds = booking_bed.available_beds+1
	booking_bed.save()
	context = {}
	context['booking'] = booking
	return render(request, 'guest/guest_cancelation.html', context)


@login_required
def guest_rating(request, pk):
	context = {}
	try:
		booking = Booking.objects.get(pk=pk, primary_person=request.user)
	except Booking.DoesNotExist:
		pass
	context['booking'] = booking
	context['form'] = RatingForm
	if request.mehtod == "POST":
		form = RatingForm(request.POSt)
		if form.is_valid():
			cleanness_rating = int(request.POST['cleanness_rating'])
			service_rating = int(request.POST['service_rating'])
			staff_rating = int(request.POST['staff_rating'])
			str(round(answer, 2))
			average = (cleanness_rating+service_rating+staff_rating)/3
			total_avg_rating = round(answer, 1)
			rating = Rating.objects.create(
				listing=booking.booking_listing,
				guest=request.user,
				cleanness_rating=cleanness_rating,
				service_rating=service_rating,
				staff_rating=staff_rating,
			)
			return redirect('thank_you_for_rating')
	return render(request, 'guest/guest_rating.html', context)
	

@login_required
@csrf_exempt
def guest_payment_failure(request):
	return render(request, 'guest/payment_failure.html')



@login_required
def guest_bookings(request):
	context = {}
	upcoming_bookings = Booking.objects.filter(primary_person=request.user,is_checkin=False, is_cancel=False)
	past_bookings = Booking.objects.filter(primary_person=request.user, is_checkin=True, is_cancel=False)
	cancled_bookings = Booking.objects.filter(primary_person=request.user, is_cancel=True, guest_cancled=True)
	context['upcoming_bookings'] = upcoming_bookings
	context['past_bookings'] = past_bookings
	context['cancled_bookings'] = cancled_bookings
	return render(request, 'guest/guest_bookings.html', context)


# def generate_hash(request, txnid):
# 	try:
# 		hash_string = get_hash_string(request,txnid)
# 		generated_hash = hashlib.sha512(hash_string.encode('utf-8')).hexdigest().lower()
# 		return generated_hash
# 	except Exception as e:
# 		print(e)
# 		return None


# def get_hash_string(request, txnid):
# 	hash_string = "{}|{}|{}|{}|{}|{}".format(
# 		config.KEY,txnid,request.session['booking_data']['total_price'],request.session['booking_data']['primary_person'],
# 		request.session['booking_data']['listing'],request.session['booking_data']['primary_person'],request.session['booking_data']['primary_person_email_id']
# 	)
# 	hash_string += config.SALT
# 	return hash_string


# def get_transaction_id():
# 	hash_object = hashlib.sha256(str(randint(0,9999)).encode("utf-8"))
# 	txnid = hash_object.hexdigest().lower()[0:32]
# 	return txnid

# def guest_booking_details(request):
# 	if request.method == 'POST':
# 		form = BookingForm(request.POST)
		
def landing_view(request):
	context =  {}
	recent_listings = Listing.objects.filter(is_active=True)[:10]
	context['recent_listings'] = recent_listings
	return render(request, 'guest/landing_index.html', context)


class GuestSignupForm(FormView):
    form_class = GuestSignupForm
    template_name = 'guest/signup.html'
    
    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:            
            return HttpResponseRedirect(reverse('guest_listings_home'))
        return super().dispatch(request, *args, **kwargs)
    
    def get_success_url(self):
        return redirect('guest_login_view')
    
    def get_form_class(self):
        return self.form_class
    
    def form_valid(self, form):
        user = User.objects.create(
        	username=self.request.POST['email_id']
        )
        user.set_password(self.request.POST['password'])
        user.save()
        guest_instance = Guest.objects.create(
        	user=user,
        	first_name=self.request.POST['first_name'],
        	last_name=self.request.POST['last_name'],
        	email_id=self.request.POST['email_id'],
        	contact_number=self.request.POST['mobile_number']
        )
        #send welcome Notification
        return HttpResponseRedirect(reverse('guest_login_view'))
    
    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))


guest_signup_view = GuestSignupForm.as_view()


class GuestLoginView(LoginView):
	template_name = 'guest/login.html'


guest_login_view = GuestLoginView.as_view()


class GuestLogoutView(View):
	next_page = reverse_lazy('guest_listings_home')

	@method_decorator(never_cache)
	def dispatch(self,request, *args, **kwargs):
		auth_logout(request)
		return HttpResponseRedirect(reverse('guest_listings_home'))

guest_logout_view = GuestLogoutView.as_view()



# import random
# def Home(request):
# 	MERCHANT_KEY = 'gtKFFx'
# 	key="gtKFFx"
# 	SALT = 'eCwWELxi'
# 	PAYU_BASE_URL = "https://sandboxsecure.payu.in/_payment"
# 	action = ''
# 	posted={}
# 	# Merchant Key and Salt provided y the PayU.
# 	for i in request.POST:
# 		posted[i]=request.POST[i]
# 	# hashlib.sha512(hash_string.encode('utf-8')).hexdigest().lower()
# 	my_secret = 'sfjesrer139290manewaw'
# 	hash_object = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()
# 	# hash_object = hashlib.sha512(my_secret.encode('utf-8')).hexdigest().lower()
# 	txnid=hash_object[0:20]
# 	hashh = ''
# 	posted['txnid']=txnid
# 	hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10"
# 	posted['key']=key
# 	hash_string=''
# 	hashVarsSeq=hashSequence.split('|')
# 	for i in hashVarsSeq:
# 		try:
# 			hash_string+=str(posted[i])
# 		except Exception:
# 			hash_string+=''
# 		hash_string+='|'
# 	hash_string+=SALT
# 	hashh = hashlib.sha256(str(hash_string).encode('utf-8')).hexdigest().lower()
# 	# hashh=hashlib.sha512(hash_string).hexdigest().lower()
# 	action =PAYU_BASE_URL
# 	context = {"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"https://test.payu.in/_payment" }
# 	if(posted.get("key")!=None and posted.get("txnid")!=None and posted.get("productinfo")!=None and posted.get("firstname")!=None and posted.get("email")!=None):
# 		return render('guest/current_datetime.html',context)
# 	else:
# 		context = {"posted":posted,"hashh":hashh,"MERCHANT_KEY":MERCHANT_KEY,"txnid":txnid,"hash_string":hash_string,"action":"." }
# 		return render('guest/current_datetime.html',context)



# @csrf_protect
# @csrf_exempt
# def success(request):
# 	c = {}
# 	c.update(csrf(request))
# 	status=request.POST["status"]
# 	firstname=request.POST["firstname"]
# 	amount=request.POST["amount"]
# 	txnid=request.POST["txnid"]
# 	posted_hash=request.POST["hash"]
# 	key=request.POST["key"]
# 	productinfo=request.POST["productinfo"]
# 	email=request.POST["email"]
# 	salt="GQs7yium"
# 	try:
# 		additionalCharges=request.POST["additionalCharges"]
# 		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
# 	except Exception:
# 		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
# 	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
# 	if(hashh !=posted_hash):
# 		print ("Invalid Transaction. Please try again")
# 	else:
# 		print ("Thank You. Your order status is ", status)
# 		print ("Your Transaction ID for this transaction is ",txnid)
# 		print ("We have received a payment of Rs. ", amount ,". Your order will soon be shipped.")
# 	return render('guest/sucess.html',{"txnid":txnid,"status":status,"amount":amount})


# @csrf_protect
# @csrf_exempt
# def failure(request):
# 	c = {}
# 	c.update(csrf(request))
# 	status=request.POST["status"]
# 	firstname=request.POST["firstname"]
# 	amount=request.POST["amount"]
# 	txnid=request.POST["txnid"]
# 	posted_hash=request.POST["hash"]
# 	key=request.POST["key"]
# 	productinfo=request.POST["productinfo"]
# 	email=request.POST["email"]
# 	salt=""
# 	try:
# 		additionalCharges=request.POST["additionalCharges"]
# 		retHashSeq=additionalCharges+'|'+salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
# 	except Exception:
# 		retHashSeq = salt+'|'+status+'|||||||||||'+email+'|'+firstname+'|'+productinfo+'|'+amount+'|'+txnid+'|'+key
# 	hashh=hashlib.sha512(retHashSeq).hexdigest().lower()
# 	if(hashh !=posted_hash):
# 		print ("Invalid Transaction. Please try again")
# 	else:
# 		print ("Thank You. Your order status is ", status)
# 		print ("Your Transaction ID for this transaction is ",txnid)
# 		print ("We have received a payment of Rs. ", amount ,". Your order will soon be shipped.")
# 	return render("guest/Failure.html",c)


from . import Checksum
from django.utils.translation import get_language

def paytm_payment(request):
	context = {}
	context['booking_data'] = request.session['booking_data']
	MERCHANT_KEY = 'ZO037#xW8fKzy5W9'
	MERCHANT_ID = 'luBdGM49434689684402'
	get_lang = "/" + get_language() if get_language() else ''
	CALLBACK_URL = settings.HOST_URL + get_lang+settings.PAYTM_CALLBACK_URL

	order_id = Checksum.__id_generator__()	
	customer_id = request.user.guest.email_id
	bill_amount = 1 #request.session['booking_data']['total_price']
	if bill_amount:
		data_dict = {
			'MID':MERCHANT_ID,
			'ORDER_ID':order_id,
			'TXN_AMOUNT': bill_amount,
			'CUST_ID': request.session['booking_data']['primary_person_email_id'],
			'INDUSTRY_TYPE_ID': 'Retail',
			'WEBSITE': 'WEBSTAGING',
			'CHANNEL_ID': 'WEB',
			'CALLBACK_URL':'http://localhost:8000/payment-response/',
		}
		param_dict = data_dict
		param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(param_dict,MERCHANT_KEY)
		print(param_dict['CHECKSUMHASH'])
		context['paytmdict'] = param_dict
		return render(request, 'guest/payment.html', context)
	return HttpResponse("Something went wrong")


@csrf_exempt
def paytm_response(request):
	if request.method == 'POST':
		MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
		data_dict = {}
		for key in request.POST:
			data_dict[key] = request.POST[key]
		verify = Checksum.verify_checksum(data_dict,MERCHANT_KEY,data_dict['CHECKSUMHASH'])
		if verify:
			Payment.objects.create(user=request.user, **data_dict)
			booking_data = request.session['booking_data']
			booking_guest = Guest.objects.get(email_id=booking_data['primary_person_email_id'])
			booking_listing = Listing.objects.get(title=booking_data['listing'])
			booking_bed=Bed.objects.get(listing=booking_listing, title=booking_data['booking_bed'])
			booking = Booking.objects.create(
				primary_person=request.user,
				booking_date=booking_data['booking_date'],
				booking_from=booking_data['checkin_time'],
				booking_to=booking_data['checkout_time'],
				number_of_guests=booking_data['total_guests'],
				is_amount_paid=True,
				total_amount=booking_data['total_price'],
				booking_bed=booking_data['booking_bed'],
				booking_listing=booking_listing,
			)
			del request.session['booking_data']
			booking_bed.available_beds = booking_bed.available_beds-1
			booking_bed.save()
			request.session.modified = True
			#booking
		else:
			return HttpResponse("checksum verify failed")
	return HttpResponse(status=200)

@login_required
def home_pay(request):
	return HttpResponse("<html><a href='"+ settings.HOST_URL +"/paytm/payment'>PayNow</html>")

@login_required
def guest_pay_at_host(request):
	context = {}
	booking_data = request.session['booking_data']
	booking_listing = Listing.objects.get(title=booking_data['listing'])
	booking_bed=Bed.objects.get(listing=booking_listing, title=booking_data['booking_bed'])
	if booking_bed.available_beds >0:
		booking_id_num = Checksum.__id_generator__()
		booking = Booking.objects.create(
			primary_person=request.user,
			booking_date=booking_data['booking_date'],
			booking_from=booking_data['checkin_time'],
			booking_to=booking_data['checkout_time'],
			number_of_guests=booking_data['total_guests'],
			is_amount_paid=True,
			total_amount=booking_data['total_price'],
			booking_bed=booking_data['booking_bed'],
			booking_listing=booking_listing,
			payment_type=Booking.P_HOST,
			booking_id_num = booking_id_num
		)
		context['booking'] = booking
		del request.session['booking_data']
		booking_bed.available_beds = booking_bed.available_beds-1
		booking_bed.save()
		request.session.modified = True
	else:
		context['error_message'] = 'Sorry! Host currently not accepting bookings!'
	return render(request, 'guest/confirmation.html', context)

# def payment(request):
# 	print("hi")
# 	context = {}
# 	context['booking_data'] = request.session['booking_data']
# 	MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
# 	MERCHANT_ID = settings.PAYTM_MERCHANT_ID
# 	get_lang = "/" + get_language() if get_language() else ''
# 	CALLBACK_URL = settings.HOST_URL + get_lang + settings.PAYTM_CALLBACK_URL
# 	order_id = Checksum.__id_generator__()
# 	bill_amount = 1
# 	if bill_amount:
# 		data_dict = {
# 			'MID':MERCHANT_ID,
# 			'ORDER_ID':order_id,
# 			'TXN_AMOUNT': bill_amount,
# 			'CUST_ID':'balaraju.mme@gmail.com',
# 			'INDUSTRY_TYPE_ID':'Retail',
# 			'WEBSITE': 'WEBSTAGING',
# 			'CHANNEL_ID':'WEB',
# 		}
# 		param_dict = data_dict
# 		param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(data_dict, MERCHANT_KEY)
# 		context['paytmdict'] = param_dict
# 		print(context)
# 		return render(request,"guest/payment.html",context)
# 	return HttpResponse("Bill Amount Could not find. ?bill_amount=10")


# @csrf_exempt
# def response(request):
# 	if request.method == "POST":
# 		MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
# 		data_dict = {}
# 		for key in request.POST:
# 			data_dict[key] = request.POST[key]
# 		verify = Checksum.verify_checksum(data_dict, MERCHANT_KEY, data_dict['CHECKSUMHASH'])
# 		if verify:
# 			return render(request,"response.html",{"paytm":data_dict})
# 		else:
# 			return HttpResponse('Checksum failed')
# 	return HttpResponse(status=200)