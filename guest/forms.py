from django import forms
from guest.models import Guest
from host.models import Area,Listing,Bed
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


class GuestSignupForm(forms.Form):
	password = forms.CharField()
	first_name = forms.CharField()
	last_name = forms.CharField()
	email_id = forms.CharField()
	mobile_number = forms.CharField()

	class Meta:
		labels = {
			'username': _("UserName"),
			'password': _("Password"),
			'first_name': _("First Name"),
			'last_name': _("Last Name"),
			'email_id': _("Email Id"),
			'mobile_number': _("Mobile Number"),
		}
	
	
	def clean_username(self):
		un = self.cleaned_data.get('username')
		un_check = User.objects.filter(username=un).exists()
		if un_check:
			raise forms.ValidationError(
				self.error_messages['invalid_username'],
				code='invalid_username',
			)
		return un

	def clean_email(self):
		email = self.cleaned_data.get('email_id')
		check = Guest.objects.filter(email_id=email).exists()
		if check:
			raise forms.ValidationError(
				self.error_messages['invalid_email_id'],
				code='invalid_email_id',
			)
		return email


class BookingForm(forms.Form):
	
	error_messages = {
		'bed_is_not_available': 'Please Choose Another Bed',
		'invalid_number_of_guests': 'Please Enter Valid Number of Guests',
	}

	booking_bed = forms.CharField()
	number_of_guests = forms.IntegerField()
	booking_date = forms.DateField()
	booking_from = forms.TimeField()
	booking_to = forms.TimeField()

	def clean_booking_bed(self):
		bed = self.cleaned_data['booking_bed']
		# b_from = self.cleaned_data['booking_from']
		# b_to = self.cleaned_data['booking_to']
		bed_check = Bed.objects.get(title=bed)
		# booking_check = Booking.objects.filter(booking_bed=bed, booking_from=b_from, b_to=b_to).exists
		if bed_check.available_beds <=0:
			raise forms.ValidationError(
				self.error_messages['bed_is_not_available'],
				code='bed_is_not_available',
			)
		# if booking_check:
		# 	raise forms.ValidationError(
		# 		self.error_messages['bed_is_not_available'],
		# 		code='bed_is_not_available',
		# 	)
		return bed

	def clean_number_of_guests(self):
		ng = self.cleaned_data['number_of_guests']
		if int(ng) <= 0:
			raise forms.ValidationError(
				self.error_messages['invalid_number_of_guests'],
				code='invalid_number_of_guests',
			)
		return ng

	def clean_booking_date(self):
		b_date = self.cleaned_data['booking_date']
		print(b_date)
		return b_date


class SearchForm(forms.Form):
	areas = Area.objects.values('area_name')
	AREA_CHOICES = [area['area_name'] for area in areas]
	LISTING_TPYES = (
	    ('Hotel', 'Hotel'),
        ('Paying Guest', 'Paying Guest'),
        ('Shared House', 'Shared House')
    )
	# search_area = forms.ChoiceField(choices=AREA_CHOICES, label='', initial='',widget=forms.Select(), required=True)
	# search_type = forms.ChoiceField(choices=LISTING_TPYES, label='', initial='', widget=forms.Select(), required=False)
	checkin_date = forms.DateField(required=True, label='')
	chekin_time = forms.TimeField(required=True, label='')
	check_out_time = forms.TimeField(required=True, label='')



class RatingForm(forms.Form):
	RATING_CHOICES = [
		('1', '1'),
		('2', '2'),
		('3', '3'),
		('4', '4'),
		('5', '5'),
	]
	cleanness_rating = forms.ChoiceField(choices=RATING_CHOICES, widget=forms.RadioSelect)
	service_rating = forms.ChoiceField(choices=RATING_CHOICES, widget=forms.RadioSelect)
	staff_rating = forms.ChoiceField(choices=RATING_CHOICES, widget=forms.RadioSelect)

	
