from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.conf.urls import url, include
from host.utils  import user_check
from django.views.generic import TemplateView

urlpatterns = [
    
    url('admin/', admin.site.urls),
    url('host/', include('host.urls')),
	url('user_check', user_check, name='user_check'),
	url('', include('guest.urls')),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
