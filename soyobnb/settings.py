

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'u-=($psw&+lbtn_i^v!x48ma6n++4@f#$4pd+(hw0okf*ld+g1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

PROJECT_APPS = [
	'host',
	'guest',
    'soyoadmin',
]

THIRD_PARTY_APPS = [
    'crispy_forms',
    # 'django_filters',
     # 'easy_thumbnails',
# 'location_field.apps.DefaultConfig', 
'django_user_agents',
]

INSTALLED_APPS = DJANGO_APPS + PROJECT_APPS + THIRD_PARTY_APPS

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # 'dare.clickjacking.XFrameOptionsMiddleware',
     # 'django_user_agents.middleware.UserAgentMiddleware',
]

ROOT_URLCONF = 'soyobnb.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'soyobnb.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
    '/var/www/static/',
]

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

CRISPY_TEMPLATE_PACK = 'bootstrap4'

# from django.core.urlresolvers import reverse_lazy

# LOGIN_REDIRECT_URL = reverse_lazy('user_check')
CRISPY_TEMPLATE_PACK = 'bootstrap4'

THUMBNAIL_ALIASES = {
    '': {
        'avatar': {'size': (50, 50), 'crop': True},
    },
}

# from django.urls import reverse_lazy
from django.core.urlresolvers import reverse_lazy

LOGIN_REDIRECT_URL = reverse_lazy('user_check')
GUEST_LOGIN_URL = reverse_lazy('guest_login_view')


# PAYMENT_MERCHANT_KEY = "ZO037#xW8fKzy5W9"
# PAYMENT_MERCHANT_ID = "luBdGM49434689684402"
# HOST_URL = "http://localhost:8080"
PAYTM_CALLBACK_URL = "/response/"


PAYTM_MERCHANT_KEY = ""
PAYTM_MERCHANT_ID = ""
HOST_URL = "http://localhost:8080"
# PAYTM_CALLBACK_URL = "/response/"
# 
if DEBUG:
    PAYTM_MERCHANT_KEY = "ZO037#xW8fKzy5W9"
    PAYTM_MERCHANT_ID = "luBdGM49434689684402"
    PAYTM_WEBSITE = 'WEB_STAGING'
    HOST_URL = 'http://localhost:8000'

