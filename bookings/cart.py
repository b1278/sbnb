from decimal import Decimal
from django.congf import settings
from host.models import Listing,Room



class Cart(object):
	
	def __init__(self, request):
		self.session = request.session
		cart = self.session.get(settings.CART_SESSION_ID)
		if not cart:
			cart = self.session[settings.CART_SESSION_ID] = {}
		self.cart = cart

	def __iter__(self):
		listing_id = self.cart.keys()
		listing = Listing.object.filter(id__in=listing_id)
		cart = self.cart.copy()

		for l in listing:
			cart[str(l.id)]['listing'] = l

		for lt in cart.values():
			yield lt


	def add_listing(self, listing, quantity=1, update_quantity=False, room_type)
		listing_id = str(listing.id)
		if listing_id not in self.cart:
			self.cart[listing_id] = {'quantity': 0}

		if update_quantity:
			self.cart[listing_id]['quantity'] = quantity
		else:
			self.cart[listing_id]['quantity'] += quantity
		self.save()

	def save(self):
		self.session.modified = True

	def remove_hotel(self, listing):
		listing_id = str(listing.id)
		if listing_id in self.cart:
			del self.cart[listing_id]
			self.save()

	def clear(self):
		del self.session[settings.CART_SESSION_ID]
		self.save()