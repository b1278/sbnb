from django.conf.urls import url
from soyoadmin import  views


urlpatterns = [
    
    url('country_create/', views.country_create_view, name='country_create_view'),
    url('countries/', views.country_list_view, name='country_list_view'),

]

