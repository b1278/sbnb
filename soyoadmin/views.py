from django.shortcuts import render
from django.views.generic import FormView,ListView,DetailView,UpdateView
from urllib.parse import urlparse, urlunparse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect, QueryDict,Http404
from django.urls import reverse_lazy
from django.shortcuts import redirect,reverse
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)
from host import models
from host import forms



class CountryCreateView(FormView):
	form_class = CountryForm()
	template_name = 'soyoadmin/country_create.html'

	def form_valid(self, form):
		instance = form.save()
		# send notification
		return self.get_success_url()

	def get_success_url(self):
		return redirect('admin_countries_view')


country_create_view = CountryCreateView.as_view()


class CountryListView(ListView):
	model = models.Country
	context_object_name = 'countries'
	template_name = 'soyoadmin/countries.html'

country_list_view = CountryListView()


class CountryDetailView(DetailView):
	model = models.Country
	context_object_name = 'country'
	template_name = 'soyoadmin/country.html'


def country_detail_view(request, pk):
	try:
		country = models.Country.objects.get(pk=pk)
	except models.Country.DoesNotExist:
		pass

	context = {}
	context['country'] = country
	
	if request.method == 'GET':
		context['form'] = forms.StateForm
		return render(request, 'soyoadmin/country.html', context)

	if request.method == 'POST':
		form = forms.StateForm(request.POST or request.FILES or None)
		if form.is_valid():
			instance = form.save()
			return redirect('country_detail_view', pk=country.id)
		return render(request, 'soyoadmin/country.html', context)



def state_detail_view(request, pk):
	try:
		state = models.State.objects.get(pk=pk)
	except models.State.DoesNotExist:
		pass

	context = {}
	context['state'] = state

	if request.method == 'GET':
		context['form'] = forms.CityForm
		return render(request, 'soyoadmin/state.html', context)

	if request.method == 'POST':
		form = forms.CityForm(request.POST or request.FILES or None)
		if form.is_valid():
			instance = form.save()
			return redirect('state_detail_view', pk=state.id)
		return render(request, 'soyoadmin/state.html', context)


def city_detail_view(request, pk):
	try:
		city = models.City.objects.get(pk=pk)
	except models.City.DoesNotExist:
		pass

	context = {}
	context['city'] = city
	
	if request.method == 'GET':
		context['form'] = forms.AreaForm()
		return render(request, 'soyoadmin/city.html', context)

	if request.method == 'POST':
		form = forms.AreaForm(request.POST or request.FILES or None)
		if form.is_valid():
			instance = form.save()
			return redirect('city_detail_view', pk=city.id)
		return render(request, 'soyoadmin/city.html', context)

		
# class StateCreateView(FormView):
# 	form_class = StateForm()
# 	template_name = 'soyoadmin/state_create.html'

# 	def form_valid(self, form):
# 		instance = form.save()
# 		# send notification
# 		return self.get_success_url()

# 	def get_success_url(self):
# 		return redirect('admin_states_view')


# class StateListView(ListView):
# 	model = models.State
# 	context_object_name = 'states'
# 	template_name = 'soyoadmin/states.html'



# class CityCreateView(FormView):
# 	form_class = CityForm()
# 	template_name = 'soyoadmin/city_create.html'

# 	def form_valid(self, form):
# 		instance = form.save()
# 		# send notification
# 		return self.get_success_url()

# 	def get_success_url(self):
# 		return redirect('admin_cities_view')


# class CityListView(ListView):
# 	model = models.City
# 	context_object_name = 'cities'
# 	template_name = 'soyoadmin/cities.html'



# class AreaCreateView(FormView):
# 	form_class = AreaForm()
# 	template_name = 'soyoadmin/area_create.html'

# 	def form_valid(self, form):
# 		instance = form.save()
# 		# send notification
# 		return self.get_success_url()

# 	def get_success_url(self):
# 		return redirect('admin_areas_view')


# class ListView(ListView):
# 	model = models.Country
# 	context_object_name = 'countries'
# 	template_name = 'soyoadmin/countries.html'
